const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const logroSchema = new Schema({
	title: {type:String, required:true},
	description: {type:String},
	author: {type:String,required:true}
});

logroSchema.methods.notificationPayload = function(){
	return {
		notification: {
			title: this.title,
			body: this.description
		}
	}
}

export const Logro = mongoose.model("Logro",logroSchema);
// var mongoose = require("mongoose");
// var Schema = mongoose.Schema;

// var img_schema = new Schema({
// 	title:{type:String,required:true},
// 	creator:{type: Schema.Types.ObjectId, ref: "User" }
// });

// var Imagen = mongoose.model("Imagen",img_schema);

// module.exports = Imagen;