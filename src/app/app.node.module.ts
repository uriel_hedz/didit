// Fix Material Support
import { __platform_browser_private__ } from '@angular/platform-browser';
function universalMaterialSupports(eventName: string): boolean { return Boolean(this.isCustomEvent(eventName)); }
__platform_browser_private__.HammerGesturesPlugin.prototype.supports = universalMaterialSupports;
// End Fix Material Support


import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { UniversalModule, isBrowser, isNode } from 'angular2-universal/node'; // for AoT we need to manually split universal packages

import {LogrosService} from './services/logros.service';
import { DBServiceNode } from './services/database.node.service';
import { NotificationServiceNode} from './services/notifications/notifications.node.service';

import { HomeModule } from './home/home.module';
import { AboutModule } from './about/about.module';
import { LogrosModule } from './logros/logros.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Cache } from './universal-cache';
import { MaterialModule } from '@angular/material';

import {AddingComponent} from './adding/adding.component';


export function getDatabase(){
  return new DBServiceNode();
}

export function getNotificationService(){
  return new NotificationServiceNode();
}

@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [ AppComponent,AddingComponent ],
  imports: [
    MaterialModule.forRoot(),
    UniversalModule, // NodeModule, NodeHttpModule, and NodeJsonpModule are included
    FormsModule,

    HomeModule,
    AboutModule,
    LogrosModule,

    AppRoutingModule
    
  ],
  providers: [
    { provide: 'isBrowser', useValue: isBrowser },
    { provide: 'isNode', useValue: isNode },
    Cache,
    LogrosService,
    {provide: 'DBService', useFactory: getDatabase},
    {provide: 'NotificationService', useFactory: getNotificationService}
  ]
})
export class MainModule {
  constructor(public cache: Cache) {

  }
  // we need to use the arrow function here to bind the context as this is a gotcha in Universal for now until it's fixed
  universalDoDehydrate = (universalCache) => {
    universalCache['Cache'] = JSON.stringify(this.cache.dehydrate());
  }
  universalAfterDehydrate = () => {
    this.cache.clear();
  }
}
