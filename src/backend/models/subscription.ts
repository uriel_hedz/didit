const mongoose = require("mongoose");
const findOrCreate = require('mongoose-findorcreate')
const Schema = mongoose.Schema;

const webpush = require('web-push');

declare var Promise;

const vapidKeys = {
  publicKey: 'BHQO2qTdLaOfXb-QSlTcXcWFmuvp7cAR_KREquMHserCyd_Sqql8ExkCeiRRt6Alr5LuAvdr0fhwIWKAqx1aSTA',
  privateKey: 'uu1cMVkNHkW0CCCjpzGToKmkKyGdNVIsMQVkF7OzymU'
};

webpush.setVapidDetails(
  'mailto:uriel@codigofacilito.com',
  vapidKeys.publicKey,
  vapidKeys.privateKey
);

const subscriptionSchema = new Schema({
	endpoint: { type: String,required:true },
	keys: {type:JSON, required:true}
});

subscriptionSchema.methods.notificationID = function(){
	return {
		endpoint: this.endpoint,
		keys: this.keys
	}
};

subscriptionSchema.methods.sendNotification = function(data){
	return webpush.sendNotification(this.notificationID(),JSON.stringify(data))
								.catch((err) => {
                	if(err.code == 'ECONNRESET' || err.statusCode === 410)
										return this.remove();
									return Promise.resolve(true);									
								})
}

subscriptionSchema.statics.sendNotifications = function(data){
	return this.find({}).then(subscriptions => {
		return subscriptions.reduce((promise,subscription)=>{
			return subscription.sendNotification(data);
		},Promise.resolve());
	})
}

subscriptionSchema.plugin(findOrCreate);

export const Subscription = mongoose.model("Subscription",subscriptionSchema);