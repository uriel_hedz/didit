module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/Users/urielhernandez/Documents/dev/angular/doit";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 47);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

module.exports = require("@angular/core");

/***/ },
/* 1 */
/***/ function(module, exports) {

module.exports = require("@angular/router");

/***/ },
/* 2 */
/***/ function(module, exports) {

module.exports = require("mongoose");

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = __webpack_require__(0);
var logro_1 = __webpack_require__(8);
var http_1 = __webpack_require__(40);
var Observable_1 = __webpack_require__(45);
exports.LogrosUrl = "/api";
var LogrosService = (function () {
    function LogrosService(http, dbService) {
        this.http = http;
        this.dbService = dbService;
    }
    LogrosService.prototype.push = function (logro) {
        console.log("Here we goooo!");
        console.log(logro);
        return this.http.post(exports.LogrosUrl + "/", logro)
            .map(this.parseLogro)
            .catch(this.handleError);
    };
    LogrosService.prototype.getSaved = function () {
        return this.dbService.get().logros
            .orderBy("id")
            .reverse()
            .toArray();
    };
    LogrosService.prototype.getLatestSavedID = function () {
        var lastLogroPromise = this.dbService.get().logros
            .orderBy("serverID")
            .reverse()
            .toArray();
        return new Promise(function (resolve, reject) {
            lastLogroPromise.then(function (lastLogro) {
                if (lastLogro.length < 1) {
                    resolve("");
                }
                else {
                    resolve(lastLogro[0].serverID);
                }
            }).catch(function (err) { return console.log(err); });
        });
    };
    LogrosService.prototype.get = function () {
        var _this = this;
        var promiseFirstSaved = this.getLatestSavedID();
        var getFromRemotePromise = promiseFirstSaved
            .then(function (id) { return _this.getRemote(id); })
            .catch(function () { return _this.getRemote(); });
        return getFromRemotePromise;
    };
    LogrosService.prototype.getRemote = function (id) {
        if (id === void 0) { id = ""; }
        var from = (id.length > 0) ? "/?from=" + id : "";
        return this.http.get(exports.LogrosUrl + from)
            .map(this.parseLogros)
            .catch(this.handleError);
    };
    LogrosService.prototype.getRemoteOldest = function (id) {
        if (id === void 0) { id = ""; }
        return this.http.get(exports.LogrosUrl + "/?since=" + id)
            .map(this.parseLogros)
            .catch(this.handleError);
    };
    LogrosService.prototype.handleError = function (error) {
        console.log(error);
        return Observable_1.Observable.throw("Algo salio mal");
    };
    LogrosService.prototype.parseLogro = function (response) {
        var logroJSON = response.json();
        return new logro_1.Logro(logroJSON.title, logroJSON.author, logroJSON.description);
    };
    LogrosService.prototype.parseLogros = function (response) {
        var data = response.json();
        return data.map(function (logroJSON) { return new logro_1.Logro(logroJSON.title, logroJSON.author, logroJSON.description, logroJSON['_id']); });
    };
    LogrosService = __decorate([
        core_1.Injectable(),
        __param(1, core_1.Inject('DBService')), 
        __metadata('design:paramtypes', [(typeof (_a = typeof http_1.Http !== 'undefined' && http_1.Http) === 'function' && _a) || Object, Object])
    ], LogrosService);
    return LogrosService;
    var _a;
}());
exports.LogrosService = LogrosService;


/***/ },
/* 4 */
/***/ function(module, exports) {

module.exports = require("express");

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var AboutComponent = (function () {
    function AboutComponent() {
    }
    AboutComponent = __decorate([
        core_1.Component({
            selector: 'about',
            template: 'About component'
        }), 
        __metadata('design:paramtypes', [])
    ], AboutComponent);
    return AboutComponent;
}());
exports.AboutComponent = AboutComponent;


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var logros_service_1 = __webpack_require__(3);
var logro_1 = __webpack_require__(8);
var router_1 = __webpack_require__(1);
var AddingComponent = (function () {
    function AddingComponent(logrosS, router) {
        this.logrosS = logrosS;
        this.router = router;
        this.logro = new logro_1.Logro("", "", "");
    }
    AddingComponent.prototype.ngOnInit = function () {
    };
    AddingComponent.prototype.submit = function () {
        var _this = this;
        this.logrosS.push(this.logro).subscribe(function (logro) {
            _this.router.navigateByUrl('');
        });
    };
    AddingComponent = __decorate([
        core_1.Component({
            selector: 'app-adding',
            template: __webpack_require__(32),
            styles: [__webpack_require__(31)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof logros_service_1.LogrosService !== 'undefined' && logros_service_1.LogrosService) === 'function' && _a) || Object, (typeof (_b = typeof router_1.Router !== 'undefined' && router_1.Router) === 'function' && _b) || Object])
    ], AddingComponent);
    return AddingComponent;
    var _a, _b;
}());
exports.AddingComponent = AddingComponent;


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var HomeComponent = (function () {
    function HomeComponent() {
    }
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'home',
            template: 'Home compnent'
        }), 
        __metadata('design:paramtypes', [])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;


/***/ },
/* 8 */
/***/ function(module, exports) {

"use strict";
"use strict";
var Logro = (function () {
    function Logro(title, author, description, serverID) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.serverID = serverID;
    }
    return Logro;
}());
exports.Logro = Logro;


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = __webpack_require__(0);
var logros_service_1 = __webpack_require__(3);
var LogrosComponent = (function () {
    function LogrosComponent(logrosS, dbService) {
        this.logrosS = logrosS;
        this.logros = [];
        this.colors = ["#3d5afe", "#fdd835", "#222", "#e64a19", "#00897b", "#ff1744", "#000", "#2979ff", "#f50057"];
        this.dbService = dbService;
    }
    LogrosComponent.prototype.oldest = function () {
        var _this = this;
        this.logrosS.getRemoteOldest(this.logros[this.logros.length - 1].serverID)
            .subscribe(function (logros) {
            _this.logros = _this.logros.concat(logros);
        });
    };
    LogrosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.logrosS.getSaved().then(function (logros) {
            console.log(logros);
            _this.logros = _this.logros.concat(logros);
        });
        this.logrosS.get().then(function (logroObservable) {
            logroObservable.subscribe(function (logros) {
                _this.dbService.sync(logros);
                _this.logros = logros.concat(_this.logros);
            });
        });
    };
    LogrosComponent = __decorate([
        core_1.Component({
            selector: 'app-logro',
            template: __webpack_require__(37),
            styles: [__webpack_require__(36), __webpack_require__(35)]
        }),
        __param(1, core_1.Inject('DBService')), 
        __metadata('design:paramtypes', [(typeof (_a = typeof logros_service_1.LogrosService !== 'undefined' && logros_service_1.LogrosService) === 'function' && _a) || Object, Object])
    ], LogrosComponent);
    return LogrosComponent;
    var _a;
}());
exports.LogrosComponent = LogrosComponent;


/***/ },
/* 10 */
/***/ function(module, exports) {

module.exports = require("@angular/material");

/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Fix Material Support
var platform_browser_1 = __webpack_require__(41);
function universalMaterialSupports(eventName) { return Boolean(this.isCustomEvent(eventName)); }
platform_browser_1.__platform_browser_private__.HammerGesturesPlugin.prototype.supports = universalMaterialSupports;
// End Fix Material Support
var core_1 = __webpack_require__(0);
var forms_1 = __webpack_require__(39);
var node_1 = __webpack_require__(42); // for AoT we need to manually split universal packages
var logros_service_1 = __webpack_require__(3);
var database_node_service_1 = __webpack_require__(26);
var notifications_node_service_1 = __webpack_require__(27);
var home_module_1 = __webpack_require__(23);
var about_module_1 = __webpack_require__(19);
var logros_module_1 = __webpack_require__(25);
var app_component_1 = __webpack_require__(21);
var app_routing_module_1 = __webpack_require__(20);
var universal_cache_1 = __webpack_require__(28);
var material_1 = __webpack_require__(10);
var adding_component_1 = __webpack_require__(6);
function getDatabase() {
    return new database_node_service_1.DBServiceNode();
}
exports.getDatabase = getDatabase;
function getNotificationService() {
    return new notifications_node_service_1.NotificationServiceNode();
}
exports.getNotificationService = getNotificationService;
var MainModule = (function () {
    function MainModule(cache) {
        var _this = this;
        this.cache = cache;
        // we need to use the arrow function here to bind the context as this is a gotcha in Universal for now until it's fixed
        this.universalDoDehydrate = function (universalCache) {
            universalCache['Cache'] = JSON.stringify(_this.cache.dehydrate());
        };
        this.universalAfterDehydrate = function () {
            _this.cache.clear();
        };
    }
    MainModule = __decorate([
        core_1.NgModule({
            bootstrap: [app_component_1.AppComponent],
            declarations: [app_component_1.AppComponent, adding_component_1.AddingComponent],
            imports: [
                material_1.MaterialModule.forRoot(),
                node_1.UniversalModule,
                forms_1.FormsModule,
                home_module_1.HomeModule,
                about_module_1.AboutModule,
                logros_module_1.LogrosModule,
                app_routing_module_1.AppRoutingModule
            ],
            providers: [
                { provide: 'isBrowser', useValue: node_1.isBrowser },
                { provide: 'isNode', useValue: node_1.isNode },
                universal_cache_1.Cache,
                logros_service_1.LogrosService,
                { provide: 'DBService', useFactory: getDatabase },
                { provide: 'NotificationService', useFactory: getNotificationService }
            ]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof universal_cache_1.Cache !== 'undefined' && universal_cache_1.Cache) === 'function' && _a) || Object])
    ], MainModule);
    return MainModule;
    var _a;
}());
exports.MainModule = MainModule;


/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var express = __webpack_require__(4);
var logro_1 = __webpack_require__(29);
var subscription_1 = __webpack_require__(30);
var ObjectId = __webpack_require__(43).ObjectID;
var router = express.Router();
router.post('/subscriptions', function (req, res) {
    console.log(req.body);
    subscription_1.Subscription.findOrCreate({ endpoint: req.body.endpoint }, req.body, function (err, subscription) {
        if (err)
            return res.json({ error: "Unable to save subscription" });
        res.json(subscription);
    });
});
// router.post('/subscriptions/remove',(req,res)=>{
//   const endpoint = req.body.endpoint;
//   Subscription.remove({endpoint: endpoint})
//           .then(result => res.json({data: {success: true}}))
//           .catch(err => res.json({error:{ message: "Could not remove"}}));
// });
router.get('/demo', function (req, res) {
    subscription_1.Subscription.find({}).then(function (subscriptions) {
        res.json(subscriptions);
    });
});
router.post('/demo', function (req, res) {
    subscription_1.Subscription.sendNotifications(req.body).then(function (result) {
        res.json(result);
    });
    // console.log(data);
    // let sendNotificationsPromise = Subscription.find({})
    //   .then(subscriptions => {
    //     // let promiseChain = Promise.resolve();
    //     // subscriptions.forEach(subscription => {
    //     //   promiseChain.then(() => {
    //     //     return subscription.sendNotification(data)
    //     //   });
    //     // });
    //     console.log(subscriptions[0]);
    //     subscriptions[0].sendNotification(data).then(response => {
    //       console.log(response);
    //       res.json({})
    //     }).catch(res.json);
    //     // promiseChain.then(()=> res.json({data: {success: true}}))
    //     //             .catch(err => res.json({error: "Could not send subscriptions" }));
    //   })
});
router.route("/:id")
    .get(function (req, res) {
    logro_1.Logro.findById(req.params.id).then(function (doc) { return res.json(doc); });
})
    .put(function (req, res) {
    var newData = {};
    if (req.body.title)
        newData["title"] = req.body.title;
    if (req.body.description)
        newData["description"] = req.body.description;
    if (req.body.author)
        newData["author"] = req.body.author;
    // new => true es necesario para obtener la última versión
    logro_1.Logro.findByIdAndUpdate(req.params.id, newData, { new: true })
        .then(function (doc) { return res.json(doc); });
})
    .delete(function (req, res) {
    logro_1.Logro.findByIdAndRemove(req.params.id)
        .then(function () { return res.json({ message: "Successfully deleted" }); });
});
router.route("/")
    .get(function (req, res) {
    var findQuery;
    if (req.query.from && req.query.from.length > 0) {
        var objectID = new ObjectId(req.query.from);
        findQuery = logro_1.Logro.find({ '_id': { $gt: objectID } });
    }
    else if (req.query.since && req.query.since.length > 0) {
        var objectID = new ObjectId(req.query.since);
        console.log(objectID);
        findQuery = logro_1.Logro.find({ '_id': { $lt: objectID } })
            .limit(10);
    }
    else {
        findQuery = logro_1.Logro.find({});
    }
    findQuery.sort({ '_id': 'desc' }).exec().then(function (doc) { return res.json(doc); });
})
    .post(function (req, res) {
    console.log(req.body);
    var logro = new logro_1.Logro({
        title: req.body.title,
        description: req.body.description,
        author: req.body.author
    });
    var logroSavedPromise = logro.save();
    var sendSubscriptionPromises = logroSavedPromise.then(function (logro) {
        return subscription_1.Subscription.sendNotifications(logro.notificationPayload());
    });
    Promise.all([logroSavedPromise, sendSubscriptionPromises])
        .then(function (_a) {
        var logro = _a[0], subscriptionResult = _a[1];
        return res.json(logro);
    })
        .catch(function (err) {
        res.json({ error: { message: "Something went wrong" } });
    });
});
exports.RouterApi = router;


/***/ },
/* 13 */
/***/ function(module, exports) {

module.exports = require("angular2-express-engine");

/***/ },
/* 14 */
/***/ function(module, exports) {

module.exports = require("angular2-universal-polyfills");

/***/ },
/* 15 */
/***/ function(module, exports) {

module.exports = require("body-parser");

/***/ },
/* 16 */
/***/ function(module, exports) {

module.exports = require("cookie-parser");

/***/ },
/* 17 */
/***/ function(module, exports) {

module.exports = require("path");

/***/ },
/* 18 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(1);
var about_component_1 = __webpack_require__(5);
var AboutRoutingModule = (function () {
    function AboutRoutingModule() {
    }
    AboutRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild([
                    { path: 'about', component: about_component_1.AboutComponent }
                ])
            ],
            exports: [
                router_1.RouterModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AboutRoutingModule);
    return AboutRoutingModule;
}());
exports.AboutRoutingModule = AboutRoutingModule;


/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var about_component_1 = __webpack_require__(5);
var about_routing_module_1 = __webpack_require__(18);
var AboutModule = (function () {
    function AboutModule() {
    }
    AboutModule = __decorate([
        core_1.NgModule({
            imports: [
                about_routing_module_1.AboutRoutingModule
            ],
            declarations: [
                about_component_1.AboutComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AboutModule);
    return AboutModule;
}());
exports.AboutModule = AboutModule;


/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(1);
var adding_component_1 = __webpack_require__(6);
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forRoot([
                    { path: 'new', component: adding_component_1.AddingComponent }
                ])
            ],
            exports: [
                router_1.RouterModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = __webpack_require__(0);
var AppComponent = (function () {
    function AppComponent(notificationService) {
        this.notificationService = notificationService;
        this.checkPermission();
    }
    AppComponent.prototype.checkPermission = function () {
        var _this = this;
        this.notificationService.isPermitted().then(function (hasPermission) {
            console.log(hasPermission);
            _this.notificationsActive = hasPermission;
        });
    };
    AppComponent.prototype.toggleNotificationPermission = function () {
        var _this = this;
        this.notificationService.togglePermission()
            .then(function () { return _this.checkPermission(); })
            .catch(console.log);
        // 
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            template: __webpack_require__(34),
            styles: [__webpack_require__(33)]
        }),
        __param(0, core_1.Inject('NotificationService')), 
        __metadata('design:paramtypes', [Object])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(1);
var home_component_1 = __webpack_require__(7);
var HomeRoutingModule = (function () {
    function HomeRoutingModule() {
    }
    HomeRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild([
                    { path: 'home', component: home_component_1.HomeComponent }
                ])
            ],
            exports: [
                router_1.RouterModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], HomeRoutingModule);
    return HomeRoutingModule;
}());
exports.HomeRoutingModule = HomeRoutingModule;


/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var home_component_1 = __webpack_require__(7);
var home_routing_module_1 = __webpack_require__(22);
var HomeModule = (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        core_1.NgModule({
            imports: [
                home_routing_module_1.HomeRoutingModule
            ],
            declarations: [
                home_component_1.HomeComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], HomeModule);
    return HomeModule;
}());
exports.HomeModule = HomeModule;


/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(1);
var logros_component_1 = __webpack_require__(9);
var LogrosRoutingModule = (function () {
    function LogrosRoutingModule() {
    }
    LogrosRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild([
                    { path: '', component: logros_component_1.LogrosComponent }
                ])
            ],
            exports: [
                router_1.RouterModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], LogrosRoutingModule);
    return LogrosRoutingModule;
}());
exports.LogrosRoutingModule = LogrosRoutingModule;


/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var common_1 = __webpack_require__(38);
var logros_component_1 = __webpack_require__(9);
var logros_routing_module_1 = __webpack_require__(24);
var material_1 = __webpack_require__(10);
var LogrosModule = (function () {
    function LogrosModule() {
    }
    LogrosModule = __decorate([
        core_1.NgModule({
            imports: [
                material_1.MaterialModule.forRoot(),
                logros_routing_module_1.LogrosRoutingModule,
                common_1.CommonModule
            ],
            declarations: [
                logros_component_1.LogrosComponent
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], LogrosModule);
    return LogrosModule;
}());
exports.LogrosModule = LogrosModule;


/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var DBServiceNode = (function () {
    function DBServiceNode() {
    }
    DBServiceNode.prototype.get = function () {
        return null;
    };
    DBServiceNode.prototype.sync = function (logros) { };
    DBServiceNode = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], DBServiceNode);
    return DBServiceNode;
}());
exports.DBServiceNode = DBServiceNode;


/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = __webpack_require__(0);
var NotificationServiceNode = (function () {
    function NotificationServiceNode() {
    }
    NotificationServiceNode.prototype.requestPermission = function () { };
    NotificationServiceNode.prototype.saveSubscription = function (data) { };
    NotificationServiceNode.prototype.isPermitted = function () {
        return Promise.resolve(false);
    };
    NotificationServiceNode.prototype.removePermission = function () { };
    NotificationServiceNode.prototype.togglePermission = function () { };
    NotificationServiceNode = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], NotificationServiceNode);
    return NotificationServiceNode;
}());
exports.NotificationServiceNode = NotificationServiceNode;


/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = __webpack_require__(0);
function rehydrateCache(defaultValue) {
    var win = window;
    if (win['UNIVERSAL_CACHE'] && win['UNIVERSAL_CACHE']['Cache']) {
        var serverCache = defaultValue;
        try {
            serverCache = JSON.parse(win['UNIVERSAL_CACHE']['Cache']);
            if (typeof serverCache !== typeof defaultValue) {
                serverCache = defaultValue;
            }
        }
        catch (e) {
            serverCache = defaultValue;
        }
        return serverCache;
    }
    return defaultValue;
}
exports.rehydrateCache = rehydrateCache;
var Cache = (function () {
    function Cache(isBrowser) {
        this._cache = {};
        if (isBrowser) {
            var serverCache = rehydrateCache(this._cache);
            this.rehydrate(serverCache);
        }
    }
    Cache.prototype.has = function (key) {
        return key in this._cache;
    };
    Cache.prototype.set = function (key, value) {
        this._cache[key] = value;
    };
    Cache.prototype.get = function (key) {
        return this._cache[key];
    };
    Cache.prototype.clear = function () {
        var _this = this;
        Object.keys(this._cache).forEach(function (key) {
            delete _this._cache[key];
        });
    };
    Cache.prototype.dehydrate = function () {
        var _this = this;
        var json = {};
        Object.keys(this._cache).forEach(function (key) {
            json[key] = _this._cache[key];
        });
        return json;
    };
    Cache.prototype.rehydrate = function (json) {
        var _this = this;
        Object.keys(json).forEach(function (key) {
            _this._cache[key] = json[key];
        });
    };
    Cache.prototype.toJSON = function () {
        return this.dehydrate();
    };
    Cache = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Inject('isBrowser')), 
        __metadata('design:paramtypes', [Boolean])
    ], Cache);
    return Cache;
}());
exports.Cache = Cache;


/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var mongoose = __webpack_require__(2);
var Schema = mongoose.Schema;
var logroSchema = new Schema({
    title: { type: String, required: true },
    description: { type: String },
    author: { type: String, required: true }
});
logroSchema.methods.notificationPayload = function () {
    return {
        notification: {
            title: this.title,
            body: this.description
        }
    };
};
exports.Logro = mongoose.model("Logro", logroSchema);
// var mongoose = require("mongoose");
// var Schema = mongoose.Schema;
// var img_schema = new Schema({
// 	title:{type:String,required:true},
// 	creator:{type: Schema.Types.ObjectId, ref: "User" }
// });
// var Imagen = mongoose.model("Imagen",img_schema);
// module.exports = Imagen; 


/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";
var mongoose = __webpack_require__(2);
var findOrCreate = __webpack_require__(44);
var Schema = mongoose.Schema;
var webpush = __webpack_require__(46);
var vapidKeys = {
    publicKey: 'BHQO2qTdLaOfXb-QSlTcXcWFmuvp7cAR_KREquMHserCyd_Sqql8ExkCeiRRt6Alr5LuAvdr0fhwIWKAqx1aSTA',
    privateKey: 'uu1cMVkNHkW0CCCjpzGToKmkKyGdNVIsMQVkF7OzymU'
};
webpush.setVapidDetails('mailto:uriel@codigofacilito.com', vapidKeys.publicKey, vapidKeys.privateKey);
var subscriptionSchema = new Schema({
    endpoint: { type: String, required: true },
    keys: { type: JSON, required: true }
});
subscriptionSchema.methods.notificationID = function () {
    return {
        endpoint: this.endpoint,
        keys: this.keys
    };
};
subscriptionSchema.methods.sendNotification = function (data) {
    var _this = this;
    return webpush.sendNotification(this.notificationID(), JSON.stringify(data))
        .catch(function (err) {
        if (err.code == 'ECONNRESET' || err.statusCode === 410)
            return _this.remove();
        return Promise.resolve(true);
    });
};
subscriptionSchema.statics.sendNotifications = function (data) {
    return this.find({}).then(function (subscriptions) {
        return subscriptions.reduce(function (promise, subscription) {
            return subscription.sendNotification(data);
        }, Promise.resolve());
    });
};
subscriptionSchema.plugin(findOrCreate);
exports.Subscription = mongoose.model("Subscription", subscriptionSchema);


/***/ },
/* 31 */
/***/ function(module, exports) {

module.exports = "md-input{\n\tdisplay: block;\n\tmargin: 1.6em 0;\n}\n\n"

/***/ },
/* 32 */
/***/ function(module, exports) {

module.exports = "<md-card>\n\t<form (submit)=\"submit()\">\n\t\t<!-- \n\t\t\t<md-input-container>\n\t\t\t\t<input name=\"title\" md-input [(ngModel)]=\"logro.title\" \n\t\t\t\trequired=\"true\">\n\t\t\t</md-input-container>\n\t\t-->\n\t\t<!-- Prioridad del equipo -->\n\t\t<!-- Nota deprecated -->\n\t\t<md-input  name=\"title\" [(ngModel)]=\"logro.title\" \n\t\trequired=\"true\" placeholder=\"Título\"></md-input>\n\t\t\n\n\t\t<md-input  name=\"author\" [(ngModel)]=\"logro.author\" \n\t\trequired=\"true\" placeholder=\"Autor\"></md-input>\n\n\t\t<md-input  name=\"description\" [(ngModel)]=\"logro.description\" placeholder=\"Descripción\"></md-input>\n\n\t\t<button md-raised-button color=\"accent\" >\n\t\t  Agregar\n\t\t</button>\n\t\t\n\t</form>\n</md-card>"

/***/ },
/* 33 */
/***/ function(module, exports) {

module.exports = ".container{\n\tmax-width: 960px;\n\tmargin:0 1em;\n\tmargin:0 auto;\n}\nmd-sidenav-layout{\n\theight: 100vh;\n}\nmd-sidenav{\n\twidth:200px;\n\tmax-width: 80%;\n}\nnav a{\n\tcolor:white !important;\n\tmargin-right: 1em;\n\tfont-size: 0.8em;\n}\nmd-sidenav a{\n\tdisplay: block;\n\ttext-align: left;\n\tpadding: 0.5em 1em;\n\tcolor:#222 !important;\n\tfont-weight: bold;\n\tmargin:0.5em 0;\n\ttext-transform: uppercase;\n}\n\nmd-sidenav a:nth-child(1){\n\tmargin-top: 2em;\n}\n.row{\n\twidth: 100%;\n}\n.pointer{\n\tcursor: pointer;\n}\n.no-padding{\n\tpadding:0px;\n}\n[app-fab]{\n\tposition: fixed;\n\tbottom:100px;\n\tright: 20px;\n\tbox-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12);\n  transform: translate3d(0, 0, 0);\n  transition: background 400ms cubic-bezier(0.25, 0.8, 0.25, 1), box-shadow 280ms cubic-bezier(0.4, 0, 0.2, 1);\n  min-width: 0;\n  border-radius: 50%;\n  width: 56px;\n  height: 56px;\n  padding: 0;\n  box-sizing: border-box;\n  cursor: pointer;\n  user-select: none;\n  outline: none;\n  border: none;\n  display: inline-block;\n  white-space: nowrap;\n  text-decoration: none;\n  vertical-align: baseline;\n  font-size: 14px;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif;\n  font-weight: 500;\n  background-color: #ff4081;\n  color:white;\n}\n[app-fab]:hover{\n\n}\n[app-fab]:active{\n\topacity: 0.8;\n\tbox-shadow: 0px 5px 5px -3px rgba(0, 0, 0, 0.2), 0px 8px 10px 1px rgba(0, 0, 0, 0.14), 0px 3px 14px 2px rgba(0, 0, 0, 0.12)\n}"

/***/ },
/* 34 */
/***/ function(module, exports) {

module.exports = "<md-toolbar color=\"primary\" >\n\t<div class=\"row\">\n\t\t<div class=\"col-xs-1 pointer no-padding text-center\" (click)=\"start.toggle()\">\n\t\t\t<md-icon> reorder </md-icon>\n\t\t</div>\n\t\t<div class=\"col-xs-5\">\n\t\t\t<a routerLink=\"/\" style=\"color:white;\"> ¡Yo hice esto! </a>\t\n\t\t</div>\n\t</div>  \n</md-toolbar>\t\n<md-sidenav-layout>\n\t<!-- #start define una variable -->\n  <md-sidenav #start>\n  \t<!-- Algunos elementos de MD rompen angular universal -->\n  \t<a routerLink=\"/home\"> Home </a>\n  \t<a routerLink=\"/about\"> About </a>\t\n  \t<a>\n  \t\t<md-checkbox [(ngModel)]=\"notificationsActive\" (change)=\"toggleNotificationPermission()\"> Notificaciones </md-checkbox>\n  \t</a>\n    <a #closeStartButton (click)=\"start.close()\">Close</a>\n  </md-sidenav>\n\n  <span *shellRender>Cargando</span>\n\t<div class=\"container\" *shellNoRender>\n\t\t<router-outlet></router-outlet>\n\t</div>\n\t<button app-fab routerLink=\"/new\">\n\t  <md-icon class=\"md-24\">add</md-icon>\n\t</button>\n\t\n</md-sidenav-layout>\n\n\n"

/***/ },
/* 35 */
/***/ function(module, exports) {

module.exports = ".amethyst{\n\tbackground: linear-gradient(#9D50BB, #6E48AA);\n}\n\n.red-mist{\n\tbackground: linear-gradient(#34e89e, #0f3443);\n}\n\n.shadow-night{\n\tbackground: linear-gradient(#457fca,#5691c8);\n}\n\n.ash{\n\tbackground: linear-gradient(#606c88,#3f4c6b);\n}\n\n.twitch{\n\tbackground: linear-gradient(#6441A5,#2a0845);\t\n}\n\n.fog{\n\tbackground: linear-gradient(#B993D6,#8CA6DB);\t\n}\n\n.strain{\n\tbackground: linear-gradient(#870000,#190A05);\t\n}\n\n.firewatch{\n\tbackground: linear-gradient(#cb2d3e,#ef473a);\t\n}\n\n.purple{\n\tbackground: linear-gradient(#41295a,#2F0743);\t\n}\n\n.sunset{\n\tbackground: linear-gradient(#ee0979,#ff6a00);\t\n}\n\n"

/***/ },
/* 36 */
/***/ function(module, exports) {

module.exports = "header{\n\tposition: relative;\n\tpadding:2em;\n\tcolor: #9fa8da;\n\tbackground-color: #222;\n}\n.top-space{\n  margin-top: 1em;\n}\n.bottom-space{\n  margin-bottom: 1em;\n}\n.logro{\n  padding:120px 20px;\n  color: #eee;\n  font-size: 1.3em;\n  position: relative;\n  text-align: center;\n}\n.logro .bottom{\n  position: absolute;\n  bottom:1em;\n  width: 100%;\n  text-align: left;\n}\n.logro h3{\n  font-size: 1.7em;\n\n}\nbutton{\n  position: relative;\n  z-index: 100;\n}\nmd-card{\n  margin-top: 1em;\n}\nheader h2{\n\tfont-size:3em;\n\ttext-align: center;\n\tdisplay: inline-block;\n\tvertical-align: baseline;\n}\n\n\n@media screen and (min-width: 40.5em) {\n  .logro {\n    width: 50%;\n    display: inline-block;\n    height: 300px;\n    margin-right: -4px;\n    vertical-align: top;\n    padding:40px 20px;\n  }\n}\n\n"

/***/ },
/* 37 */
/***/ function(module, exports) {

module.exports = "<div class='logro' *ngFor=\"let logro of logros;let i = index\"[style.background]=\"colors[(i+1) % colors.length ]\">\n\t<h3>{{logro.title}}</h3>\n\t<p>{{logro.description}}</p>\n\t<div class=\"bottom\">\n\t\t<p class=\"author\">{{logro.author}}</p>\t\n\t</div>\n\t\n</div>\n<div class=\"top-space bottom-space text-center\">\n\t<button (click)=\"oldest()\" md-raised-button color=\"accent\" >Cargar más</button>\n</div>\n"

/***/ },
/* 38 */
/***/ function(module, exports) {

module.exports = require("@angular/common");

/***/ },
/* 39 */
/***/ function(module, exports) {

module.exports = require("@angular/forms");

/***/ },
/* 40 */
/***/ function(module, exports) {

module.exports = require("@angular/http");

/***/ },
/* 41 */
/***/ function(module, exports) {

module.exports = require("@angular/platform-browser");

/***/ },
/* 42 */
/***/ function(module, exports) {

module.exports = require("angular2-universal/node");

/***/ },
/* 43 */
/***/ function(module, exports) {

module.exports = require("mongodb");

/***/ },
/* 44 */
/***/ function(module, exports) {

module.exports = require("mongoose-findorcreate");

/***/ },
/* 45 */
/***/ function(module, exports) {

module.exports = require("rxjs/Observable");

/***/ },
/* 46 */
/***/ function(module, exports) {

module.exports = require("web-push");

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {"use strict";
// the polyfills must be one of the first things imported in node.js.
// The only modules to be imported higher - node modules with es6-promise 3.x or other Promise polyfill dependency
// (rule of thumb: do it if you have zone.js exception that it has been overwritten)
__webpack_require__(14);
var path = __webpack_require__(17);
var express = __webpack_require__(4);
var bodyParser = __webpack_require__(15);
var cookieParser = __webpack_require__(16);
var mongoose = __webpack_require__(2);
var connectionString = process.env.MONGODB_URI ||
    process.env.MONGOLAB_URI ||
    process.env.MONGOHQ_URL ||
    'mongodb://localhost/logros';
mongoose.connect(connectionString);
// Angular 2
var core_1 = __webpack_require__(0);
// Angular 2 Universal
var angular2_express_engine_1 = __webpack_require__(13);
// App
var app_node_module_1 = __webpack_require__(11);
// enable prod for faster renders
core_1.enableProdMode();
var app = express();
var ROOT = path.join(path.resolve(__dirname, '..'));
// Express View
app.engine('.html', angular2_express_engine_1.createEngine({
    precompile: true,
    ngModule: app_node_module_1.MainModule
}));
app.set('views', __dirname);
app.set('view engine', 'html');
app.use(cookieParser('Angular 2 Universal'));
app.use(bodyParser.json());
// Serve static files
app.use('/assets', express.static(path.join(__dirname, 'assets'), { maxAge: 30 }));
app.use(express.static(path.join(ROOT, 'dist/client'), { index: false }));
var api_1 = __webpack_require__(12);
// Our API for demos only
app.use("/api", api_1.RouterApi);
function ngApp(req, res) {
    res.render('index', {
        req: req,
        res: res,
        preboot: false,
        baseUrl: '/',
        requestUrl: req.originalUrl,
        originUrl: 'http://localhost:3000'
    });
}
// Routes with html5pushstate
// ensure routes match client-side-app
app.get('/', ngApp);
app.get('/about', ngApp);
app.get('/about/*', ngApp);
app.get('/home', ngApp);
app.get('/logros', ngApp);
app.get('/logros/*', ngApp);
app.get('/home/*', ngApp);
app.get('/new', ngApp);
app.get("/sw", function (req, res) {
    console.log(">:(");
    console.log(path.join(__dirname, 'assets', 'sw.js'));
    res.sendFile('assets/sw.js', { root: __dirname });
});
app.get("/manifest", function (req, res) {
    res.sendFile('assets/manifest.webmanifest', { root: __dirname });
});
app.get('*', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    var pojo = { status: 404, message: 'No Content' };
    var json = JSON.stringify(pojo, null, 2);
    res.status(404).send(json);
});
// Server
var server = app.listen(process.env.PORT || 3000, function () {
    console.log("Listening on: http://localhost:" + server.address().port);
});

/* WEBPACK VAR INJECTION */}.call(exports, "src"))

/***/ }
/******/ ]);