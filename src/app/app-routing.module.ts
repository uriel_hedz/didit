import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {AddingComponent} from './adding/adding.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
    	{ path: 'new', component: AddingComponent }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
