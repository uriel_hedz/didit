import { Component,OnInit } from '@angular/core';
import { LogrosService } from '../services/logros.service';
import { Logro } from '../logros/logro';
import {Router} from '@angular/router';

@Component({
  selector: 'app-adding',
  templateUrl: 'adding.component.html',
  styleUrls: ['adding.component.css']
})
export class AddingComponent implements OnInit {
	
	logro : Logro = new Logro("","","");	

	constructor(private logrosS : LogrosService,private router : Router){}

	ngOnInit(){
	}

	submit(){
		this.logrosS.push(this.logro).subscribe(logro =>{
			this.router.navigateByUrl('');
		});
	}

}
