import { Component,OnInit,Inject} from '@angular/core';
import { LogrosService } from '../services/logros.service';
import { Logro } from './logro';
import { Observable } from 'rxjs';
import { NotificationService } from '../services/notifications/notifications.abstract.service'

@Component({
  selector: 'app-logro',
  templateUrl: 'logros.component.html',
  styleUrls: ['./logros.component.css','./colors.css']
})
export class LogrosComponent implements OnInit {
	dbService : any;
	notificationService : NotificationService;
	logros : Logro[] = [];

	colors : string[] = ["#3d5afe","#fdd835","#222","#e64a19","#00897b","#ff1744","#000","#2979ff","#f50057"];

	constructor(private logrosS : LogrosService, 
							@Inject('DBService') dbService : any
							){
		
		this.dbService = dbService;
		
	}

	oldest(){
		this.logrosS.getRemoteOldest(this.logros[this.logros.length - 1].serverID)
						.subscribe((logros)=>{
							this.logros = this.logros.concat(logros);
						})
	}

	ngOnInit(){

		

		this.logrosS.getSaved().then((logros)=>{
			console.log(logros);
			this.logros = this.logros.concat(logros);
		})

		this.logrosS.get().then((logroObservable)=>{
						
			logroObservable.subscribe((logros)=>{
				this.dbService.sync(logros);
				this.logros = logros.concat(this.logros);
			});
		

		});

	}

	

}
